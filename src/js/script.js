const burgerMenu = document.querySelector('.header__btn-burger');
const navList = document.querySelector('.header__nav-list');
burgerMenu.classList.remove('header__btn-burger--active');
burgerMenu.addEventListener("click", () => {
    burgerMenu.classList.toggle('header__btn-burger--active');
    navList.classList.toggle('header__nav-list--hide');
});