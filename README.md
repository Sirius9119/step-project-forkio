# step project FORKIO

## Getting started

Start of project 30.03.2023

## Integrate with your tools

Gulp + SASS

"browser-sync": "^2.29.1",
"gulp": "^4.0.2",
"gulp-autoprefixer": "^8.0.0",
"gulp-clean": "^0.4.0",
"gulp-clean-css": "^4.3.0",
"gulp-concat": "^2.6.1",
"gulp-htmlmin": "^5.0.1",
"gulp-imagemin": "^7.1.0",
"gulp-sass": "^5.1.0",
"gulp-uglify": "^3.0.2",
"sass": "^1.59.3"
"gulp-file-include": "^2.3.0"

## Description
Step project 2 for Dan.IT education

## Authors and acknowledgment
Team members:
Artem Lyashenko - made section "Header" and "Peaple-Talking", burger menu using JS
Yaroslav Havtylenko - made section "Revolutionery", "Getting" and "Pricing"

## Project status
Done 09.04.2023